package com.expenses.calculator.domain;

public class CategoryImportance {

    private final int value;

    public CategoryImportance(int value) {
        doValidation(value);
        this.value = value;
    }

    private void doValidation(int value) {
        if (value > 10 || value < 0)
            throw new IllegalStateException("Value cant be greater than 10 or lower than 0");
    }

    public int getValue() {
        return value;
    }

}
