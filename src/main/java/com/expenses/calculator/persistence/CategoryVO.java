package com.expenses.calculator.persistence;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "category")
public class CategoryVO {

    @Id
    private String categoryid;
    private String categoryname;
    private String categoryimportance;

    public CategoryVO() {
    }

    public CategoryVO(String categoryid, String categoryname, String categoryimportance) {
        this.categoryid = categoryid;
        this.categoryname = categoryname;
        this.categoryimportance = categoryimportance;
    }

    public String getCategoryid() {
        return categoryid;
    }

    public String getCategoryname() {
        return categoryname;
    }

    public String getCategoryimportance() {
        return categoryimportance;
    }

    public void setCategoryid(String categoryid) {
        this.categoryid = categoryid;
    }

    public void setCategoryname(String categoryname) {
        this.categoryname = categoryname;
    }

    public void setCategoryimportance(String categoryimportance) {
        this.categoryimportance = categoryimportance;
    }
}
