package com.expenses.calculator.domain;

public enum CategoryError {
    INVALID_NAME,
    INVALID_IMPORTANCE
}
