package com.expenses.calculator.application;

import com.expenses.calculator.domain.Category;
import com.expenses.calculator.domain.CategoryImportance;
import com.expenses.calculator.domain.CategoryName;
import com.expenses.calculator.infrastructure.CategoryRequest;
import com.expenses.calculator.persistence.CategoryRepository;
import org.springframework.stereotype.Service;

@Service
public class CategoryCreator {

    private final CategoryRepository categoryRepository;

    public CategoryCreator(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    public String create(CategoryRequest categoryRequest) {

        Category category = mapCategory(categoryRequest);

        return categoryRepository.save(category);
    }

    private Category mapCategory(CategoryRequest categoryRequest) {
        return new Category(
                new CategoryName(categoryRequest.categoryName),
                new CategoryImportance(categoryRequest.categoryImportance)
        );
    }
}
