DROP TABLE IF EXISTS `category`;

CREATE TABLE `category`
(
 `categoryid` varchar(255),
 `categoryname` varchar(255),
 `categoryimportance` varchar(255),

  PRIMARY KEY (`categoryid`)
);
