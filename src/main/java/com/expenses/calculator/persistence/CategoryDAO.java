package com.expenses.calculator.persistence;

import org.springframework.data.repository.CrudRepository;

interface CategoryDAO extends CrudRepository<CategoryVO, String> {}
