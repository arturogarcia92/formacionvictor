package com.expenses.calculator.domain;

public class CategoryName {

    private final String value;

    public CategoryName(String value) {
        doValidation(value);
        this.value = value;
    }

    private void doValidation(String value) {
        if (value.length() > 10)
            throw new IllegalStateException("Value cant be greater than 10 characters");
    }

    public String getValue() {
        return value;
    }
}
