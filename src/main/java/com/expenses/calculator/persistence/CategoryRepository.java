package com.expenses.calculator.persistence;

import com.expenses.calculator.domain.Category;
import org.springframework.stereotype.Repository;

@Repository
public class CategoryRepository {

    private final CategoryDAO categoryDAO;

    public CategoryRepository(CategoryDAO categoryDAO) {
        this.categoryDAO = categoryDAO;
    }

    public String save(Category category) {
        return categoryDAO.save(mapCategoryVO(category)).getCategoryid();
    }

    private CategoryVO mapCategoryVO(Category category) {
        return new CategoryVO(
                category.getCategoryId(),
                category.getCategoryName().getValue(),
                mapCategoryImportance(category.getCategoryImportance().getValue())
        );
    }

    private String mapCategoryImportance(int value) {
        if (value < 5 && value > 0) {
            return "LOW";
        }
        if (value >= 5 && value < 10)
            return "HIGH";
        else
            throw new IllegalStateException("Value not allowed");
    }
}
