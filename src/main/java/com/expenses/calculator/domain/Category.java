package com.expenses.calculator.domain;

import java.util.UUID;

public class Category {

    private final String categoryId;
    private final CategoryName categoryName;
    private final CategoryImportance categoryImportance;

    public Category(CategoryName categoryName, CategoryImportance categoryImportance) {
        this.categoryId = UUID.randomUUID().toString();
        this.categoryName = categoryName;
        this.categoryImportance = categoryImportance;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public CategoryName getCategoryName() {
        return categoryName;
    }

    public CategoryImportance getCategoryImportance() {
        return categoryImportance;
    }
}
