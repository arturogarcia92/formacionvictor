package com.expenses.calculator.infrastructure;

public class CategoryRequest {

    public String categoryName;
    public int categoryImportance;

    public CategoryRequest(String categoryName, int categoryImportance) {
        this.categoryName = categoryName;
        this.categoryImportance = categoryImportance;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public int getCategoryImportance() {
        return categoryImportance;
    }

}
