package com.expenses.calculator.infrastructure;

import com.expenses.calculator.application.CategoryCreator;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CategoryCreatorController {

    private final CategoryCreator categoryCreator;

    public CategoryCreatorController(CategoryCreator categoryCreator) {
        this.categoryCreator = categoryCreator;
    }

    @PostMapping("/category")
    public void createCategory(@RequestBody CategoryRequest categoryRequest) {
        categoryCreator.create(categoryRequest);
    }

}
